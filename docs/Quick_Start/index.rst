Quick Start
-----------

The following two notebooks just quickly summarize the most important functionalities that
postopus provides. The first one generates a simple example on the fly and is kept as short as possible.
The second one uses a more sophisticated example, therefore the plots are fancier. It also explains more details than the first one.
Nonetheless, if you want to dive deeper we recommend visiting the `User Guide
<../User_Guide/index.html>`_

.. toctree::
    Quickstart with postopus <Quick_Start>
    Detailed Overview <Detailed_Overview>
