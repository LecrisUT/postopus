User Guide
----------

.. toctree::

   Plotting <Plotting>
   Data Exploration with Holoviews <exploration_with_holoviews>
   xrft usage for FFTs with xarray <xrft>
   units <units>
