========
postopus
========

This is the documentation of `Postopus <https://gitlab.com/octopus-code/postopus/>`_ (the POST-processing of OctoPUS data), which is an environment that
can help with the analysis of data that was computed using the `Octopus <https://octopus-code.org>`_ TDDFT package.

Installation
============
For installing the basic dependencies of postopus: ``pip install postopus``. Although for interacting with the tutorial notebooks a ``pip install postopus[recommended]`` is needed. This will install packages for analysis and plotting like ``matplotlib``, ``holoviews`` and ``jupyter`` among others.
If you want to contribute, develop, or build the documentation locally, please follow the instructions in the `README <https://gitlab.com/octopus-code/postopus/#developer-setup>`_.


Contents
========

.. toctree::
   :maxdepth: 2

   Quick Start <Quick_Start/index>
   User Guide <User_Guide/index>
   License <license>
   Change log <changelog>
   FAQ <faq>
   Module Reference <api/modules>





Indices and tables
==================

* :ref:`genindex`


About this documentation
========================

.. note::

    This is still work in progress.
