{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e067c68a",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "Postopus is a post-processing tool for [Octopus](https://octopus-code.org/) (POSTprocessing for OctoPUS). It provides a user-friendly interface to find and read data written by Octopus throughout a simulation and offers common operations to evaluate this data.\n",
    "\n",
    "## Loading Data with Postopus\n",
    "To load data with Postopus the path to the output directory of the Octopus simulation is required. In this folder, all output data, as well as the input file `inp` are expected. Data is found automatically and can be discovered by the user by listing all found systems/fields/etc or using auto-completion at run time, e. g. when using Jupyter Notebook.\n",
    "\n",
    "The entry point for users to Postopus is the `Run` class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c423391",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "\n",
    "from postopus import Run\n",
    "\n",
    "# Path to some example data\n",
    "path_to_octopus_output = Path(\"\").resolve().parents[1] / \"tests\" / \"data\" / \"archimedean_spiral\"\n",
    "\n",
    "# Instantiate Run object\n",
    "run = Run(path_to_octopus_output)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9780128",
   "metadata": {},
   "source": [
    "The `Run` object discovers available data on the file system and builds a data structure allowing access. In general, the data structure allows choosing data with the following syntax:  \n",
    "\n",
    "Scalar fields: run.*systemname*.*calculationmode*.*fieldname*  \n",
    "Vector fields: run.*systemname*.*calculationmode*.*fieldname*(.*dimension*) \n",
    "\n",
    "Parameters set in italics must be replaced with values that mostly correspond to values set in the input file. A closer look at those will be taken in the following sections.\n",
    "\n",
    "For reference, see the input file for the simulation used in this example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "135183de",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(path_to_octopus_output / \"inp\") as f:\n",
    "    print(\"\".join(f.readlines()))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81bc0aea",
   "metadata": {},
   "source": [
    "While the input file for this example is quite complex, it allows to check out all features and therefore is used.\n",
    "\n",
    "## System selection\n",
    "\n",
    "The first parameter to select is the system's name. Octopus allows to simulate multiple systems at once, as well as so-called \"multisystem\"s which build a hierarchy of encapsulated systems.  \n",
    "Checking out the \"Systems\" block in the `inp`, two systems can be found:  \n",
    "```\n",
    "%Systems\n",
    "  'Maxwell' | maxwell\n",
    "  'Medium'  | linear_medium\n",
    "%\n",
    "```  \n",
    "One system with the name \"Maxwell\" of type \"maxwell\" and a system \"Medium\" of type \"linear_medium\". The types here are relevant for Octopus, for us the system names are of interest.  \n",
    "Be aware that simulation with Octopus is also possible without setting any systems. In that case, the system's type will be set (by Octopus) to \"electronic_system\". As Postopus requires a name for this system, it will be named \"**default**\" (while not having a name in Octopus). Also, the \"default\" system will always exist, as it is used to store global parameters read from the `inp`, but will never contain any data when the \"Systems\" block is defined in `inp`.\n",
    "\n",
    "To load data from a system, we now have two choices: either access `run.Maxwell` or `run.Medium`.  \n",
    "Besides reading these names from the `inp`, it also is possible to access this via Postopus. Use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ed6d1e06",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.systems.keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5a330c1",
   "metadata": {},
   "source": [
    "## System data - Calculation modes and subsytems\n",
    "\n",
    "To see which calculation modes are available, one could use the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "691a7ec5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Maxwell system\n",
    "run.Maxwell.system_data.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6afc9bed",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Medium system\n",
    "run.Medium.system_data.keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d4a9a2d",
   "metadata": {},
   "source": [
    "This is expected, as the `CalculationMode` variable in the `inp` is set to \"td\". As the time-dependent calculation (\"td\") required a previous self-consistent field simulation (\"scf\") this data also could be present in the output folder. If this would be the case, one could see `dict_keys(['td', 'scf'])` as output and select between these two. For multisystem examples like the `celestial_bodies` tutorial, we would also have the keys `Moon`, `Earth` and `Sun` as subsystems."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ebb2d4d",
   "metadata": {},
   "source": [
    "## Fields\n",
    "\n",
    "Getting a list of all available fields can be done with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "292d2e0e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Maxwell system\n",
    "run.Maxwell.td.fields.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "da6d99b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Medium system\n",
    "run.Medium.td.fields.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9a90a2da-4cf8-46b5-9e60-e238945a3c16",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.Maxwell.td.maxwell_energy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25ae4705",
   "metadata": {},
   "source": [
    "\n",
    "In this simulation, the Medium system does not produce any output, as only `MaxwellOutput` is configured in the `inp`, but no `Output` block is present.\n",
    "\n",
    "As this is a time-dependent simulation and `MaxwellTDOutput` is set, Octopus produces a folder `td.general` on the file system with data that is a field for a specific iteration, but a scalar value for all iterations. In this example, this is the case for \"total_b_field\" as well as \"total_e_field\" and \"maxwell_energy\". These outputs can represent a scalar field, e.g. \"maxwell_energy\" or a vector field e.g. \"total_b_field\". The latter is handled as `TDGeneralVectorFields`, which include three scalar fields, one for each dimension. The scalar fields that are stored within `td.general` (both as one dimension of `TDGeneralVectorFields` and the single fields like the \"maxwell_energy\") are not handled as `Fields` in Postopus, but as `Pandas DataFrames`, as they are only text and line-based outputs of a single scalar value with no spatial dimension. The `td.general` fields have a `.attrs` attribute, where one can inspect the metadata associated with the field, as well as the units associated with each column of the dataframe. (The `static` files that are read as `pandas.Dataframes` have also an `.attrs` attribute)\n",
    "\n",
    "Postopus differentiates between two types of fields: Vector fields and scalar fields. Checking the type of a field can be done with Python's `type` built-in.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3a884529",
   "metadata": {},
   "outputs": [],
   "source": [
    "type(run.Maxwell.td.e_field)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a90237a5",
   "metadata": {},
   "source": [
    "In this example, the Maxwell output only contains vector fields. What sets vector and scalar fields apart in Postopus is that a `VectorField` in Postopus contains three `ScalarFields`. This is because Octopus produces the output of a vector field in the three files, each containing a vector component in one of the cardinal dimensions of space (most often `x`, `y` and `z`). To access a dimension, simply provide the name of the desired axis:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e082c642",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Scalar field containing values in x direction\n",
    "type(run.Maxwell.td.e_field.x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15579f6e",
   "metadata": {},
   "source": [
    "## Loading Data\n",
    "\n",
    "After we have discovered all available data, we finally want to load the values.\n",
    "\n",
    "### Get Data\n",
    "\n",
    "Accessing data in NumPy format can be done with a call to the `get()` method. `get()` takes two parameters, `step` for the iteration number you want to access and `source` for the source of data. If we are dealing with `td` data, the `get` function will transform the `step` into time `t` (`step *TDTimestep` (from parser.log)).\n",
    "Our example `inp` has defined `MaxwellOutputInterval` (also could be `OutputInterval`) with a value of 50, meaning Octopus will write all fields every 10 simulation steps. The available steps for a field (e. g. \"e_field\") can be checked with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "50de394e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Available steps for e_field in Maxwell system\n",
    "run.Maxwell.td.e_field.z.iteration_ids"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af981bbc",
   "metadata": {},
   "source": [
    "If we now would like to load the e_field in z direction at the plane xy at z=0 at simulation step 350, one would call:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "199930e1",
   "metadata": {},
   "outputs": [],
   "source": [
    "e_field_plane = run.Maxwell.td.e_field.z.get([350, 400], source=\"z=0\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65770ed5",
   "metadata": {},
   "source": [
    "The returned `Field` itself is an `xarray`. the `xarray` has the following attributes  \n",
    "    - `values` contains the data as a `NumPy` array  \n",
    "    - `coords` provides the correct spacial coordinates for every data point in `values`  \n",
    "    - `dims` gives the number of dimensions for the data, as well as the dimension names  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "105f49cc-6f07-4b9f-922c-37a8310438c5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Field\n",
    "e_field_plane"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9894cbcc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Actual field values\n",
    "e_field_plane.values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46765663",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Shape of the output\n",
    "e_field_plane.values.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d41f9fe1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Dimensions in the values field\n",
    "e_field_plane.dims"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b576ad2f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# coordinates of the values\n",
    "e_field_plane.coords"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "95caba8e",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(e_field_plane.coords[\"x\"].shape)\n",
    "print(e_field_plane.coords[\"y\"].shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6dfc7c39",
   "metadata": {},
   "source": [
    "Plotting this could now be done with Matplotlib's `imshow()` or `countour()` (or one could use `xarray`'s plot or `holoviews`). More information in [Plotting Documentation](xarray-plots1.ipynb). Note that selecting a time step is required here, as `imshow()` expects two-dimensional data to plot."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8be55c9",
   "metadata": {},
   "source": [
    "The `source` parameter can be omitted, if there is only one source (file-extension). Postopus would select the only available extension available, for the requested files. If there is more than one source, postopus will throw a `ValueError`, showing the user the different available sources:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1c10998",
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [],
   "source": [
    "e_field_plane_auto = run.Maxwell.td.e_field.z.get([350, 400])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56ef25fb",
   "metadata": {},
   "source": [
    "You can also select the data by index with the `iget` method. The `indices` parameter can be also negative, like in python `list`s. One can also `iget` a `list` of indices or a `slice`. This method could come handy in case you don't want to look up the step number of the last iteration for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d5452ecf",
   "metadata": {},
   "outputs": [],
   "source": [
    "e_field_plane_indx = run.Maxwell.td.e_field.z.iget(-1, \"z=0\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bbe9b74a",
   "metadata": {},
   "outputs": [],
   "source": [
    "e_field_plane_indx"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a95cb37b",
   "metadata": {},
   "source": [
    "The `Field` above is identical as the following one, which holds the data for the last iteration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f08e667",
   "metadata": {},
   "outputs": [],
   "source": [
    "e_field_plane_ = run.Maxwell.td.e_field.z.get(800, source=\"z=0\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "780aa7ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "e_field_plane"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7e591224",
   "metadata": {},
   "source": [
    "### Getting Data over all Simulation Steps\n",
    "\n",
    "For more complex data operations, as well as loading all simulation steps in the output, `get_all(...)` provides a xarray.DataArray that contains this data. It takes a parameter `source`, similar to the `get()` method. Be aware that this method has quite a long runtime. (For `ScalarField`s with an `scf` calculation mode, we can also call `get_converged()`, which returns the converged `scf` iteration data.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58a21378",
   "metadata": {},
   "outputs": [],
   "source": [
    "e_field_over_time = run.Maxwell.td.e_field.z.get_all(\"z=0\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "afa053fe",
   "metadata": {},
   "outputs": [],
   "source": [
    "e_field_over_time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d1b0596",
   "metadata": {},
   "outputs": [],
   "source": [
    "type(e_field_over_time)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "916c0fed-62ca-4f1e-b4a4-7c54359ffdb6",
   "metadata": {},
   "source": [
    "## Plotting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "283c4989",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "plt.imshow(e_field_plane.sel(t=0.3686, method=\"nearest\").values)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f4a5ac8",
   "metadata": {},
   "source": [
    "Xarray provides plotting custom methods, as well as options to slice data. [Plotting Documentation](xarray-plots1.ipynb). Without addition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "620b6545",
   "metadata": {},
   "outputs": [],
   "source": [
    "e_field_plane.sel(t=0.3686, method=\"nearest\").plot(x=\"x\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "218f28e2-15e2-4251-88a9-e796ba75d90b",
   "metadata": {},
   "source": [
    "As said above, using `matplotlib` or `xarray` itself is well suited for doing 2D static plots. For more dynamic and/or higher dimensional plots, we recommend using `holoviews` s. [Holoviews tutorial](holoviews_with_postopus.ipynb)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30441f2b",
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "hv.extension('bokeh')  # Allow for interactive plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "66453672-df5c-4001-8c2b-f110fe0ebd68",
   "metadata": {},
   "outputs": [],
   "source": [
    "# For customizing the plot, s. the holoviews tutorial\n",
    "# Note for web users: You should have an active notebook session to interact with the plot\n",
    "hv_ds = hv.Dataset(e_field_over_time)\n",
    "hv_im = hv_ds.to(hv.Image, kdims=[\"x\", \"y\"], dynamic=True)\n",
    "hv_im"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
