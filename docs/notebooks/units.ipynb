{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "3d54bdf1-307e-435f-b04d-fc4f9cc3b18e",
   "metadata": {},
   "source": [
    "# Units"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "49480a1e-04b3-4a13-b113-7b20309ac9fa",
   "metadata": {},
   "source": [
    "Internally, octopus uses atomic units for computing values. Nonetheless, the user can choose the output units with the `inp` parameter [UnitsOutput](https://octopus-code.org/new-site/develop/variables/execution/units/unitsoutput/). The default is `atomic` [units], but one can use `ev_angstrom` as well. In the latter case, the spatial dimensions will be measured in Angstrom and the energies will be measured in `eV`. In the `atomic` case, the spatial dimensions will be measured in bohr, while all the other variables will be measured in atomic units. More on this on the [wiki](https://octopus-code.org/new-site/develop/variables/execution/units/) of octopus. The election of the `inp` parameter of the user is reflected in the `parser.log` after the run is finished. We retrieve the unit information from there.\n",
    "\n",
    "We will compare the benzene and methane test examples."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7740d105-21aa-4155-9e46-455002bb028e",
   "metadata": {},
   "source": [
    "## Methane example (atomic)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f890bffc-248c-4d9a-b6d1-727ebcb0c4ff",
   "metadata": {},
   "source": [
    "For the methane example, we've left the default atomic units, internally Octopus interprets it as a 0:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7918ba4a-6bb9-4845-bb1a-bb8211e43df2",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd ../../tests/data/methane/exec"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9d1b852e-dd8e-4eae-b8b9-2223e624518c",
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 100 parser.log | grep \"UnitsOutput\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "768bb436-ede7-4884-a7c1-1292ac564fef",
   "metadata": {},
   "source": [
    "We load the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e3386e56-48de-4204-9942-c1f6352ed158",
   "metadata": {},
   "outputs": [],
   "source": [
    "from postopus import Run\n",
    "import holoviews as hv\n",
    "from holoviews import opts  # For setting defaults\n",
    "hv.extension('bokeh', 'matplotlib')  # Allow for interactive plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1fb67a3f-708b-4b10-a798-e63d97b14348",
   "metadata": {},
   "outputs": [],
   "source": [
    "benzene_path = \"../\" # benzene root folder\n",
    "run = Run(benzene_path)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "893ddf45-07f1-45be-8753-41430d96c54f",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa = run.default.td.density.get([1,2],source=\"ncdf\")\n",
    "xa"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9cefc8b3-c58f-4d87-96c6-2f65d35783de",
   "metadata": {},
   "source": [
    "Here, we can see that the `xarray` has the units `au`. In the following we will show how to access these units and also the units of the individual coordinates:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3a8be7a8-4622-4a3e-857c-ac6ab94e9b70",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa.units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b5c2ecb-da72-4464-ab22-9ecdb9784d29",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa.y.units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9621451b-27b0-4587-b0ea-cfba13558c7c",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa.t.units"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "57a255cb-5289-4d11-9a3a-94f3245e686c",
   "metadata": {},
   "source": [
    "If we use holoviews for plotting, the coordinates are automatically labeled according to the units of the `xarray`. Unfortunately, labeling the color bar needs to be done manually. For more information about plotting with holoviews, look at the [holoviews]((holoviews_with_postopus.ipynb) tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0ec15d86-d6eb-433d-aa87-105c8e09717a",
   "metadata": {},
   "outputs": [],
   "source": [
    "hv_ds = hv.Dataset(xa)\n",
    "hv_im = hv_ds.to(hv.Image, kdims=[\"x\", \"y\"], dynamic=True)\n",
    "hv_im.opts(colorbar=True,\n",
    "                    width=500,\n",
    "                    height=400,\n",
    "                    cmap='seismic',\n",
    "                    clabel=f\"{xa.name} ({xa.units})\",)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8f148eca-2721-40b7-a10f-647a8f152bb2",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Benzene example (ev_angstrom)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "436ec1e5-e189-4025-95c1-b97385978327",
   "metadata": {},
   "source": [
    "For the benzene example, we specified `UnitsOutput = ev_angstrom` in the `inp` file. This is interpreted as the number 1 internally in Octopus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dadf7015-aba4-4dbc-87fc-714191c99eb5",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd ../../benzene/exec"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aefef029-9f37-494b-a403-0ffa6965ed7d",
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 100 parser.log | grep \"UnitsOutput\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c251250-028a-42bf-a211-635d897f0d8c",
   "metadata": {},
   "outputs": [],
   "source": [
    "benzene_path = \"..\" # methane path\n",
    "runb = Run(benzene_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eecbc057-992c-49aa-84d4-6e5c5b96dd2f",
   "metadata": {},
   "source": [
    "We will do the same exploration as we did before with methane:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc136a3f-2986-43d2-a189-0510b9a9fb12",
   "metadata": {},
   "outputs": [],
   "source": [
    "xab = runb.default.scf.density.get_converged(source=\"ncdf\") \n",
    "xab"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec1bc29b-126e-4dbb-942d-3f6ea9351793",
   "metadata": {},
   "outputs": [],
   "source": [
    "xab.units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7158aad-4f0c-4870-b60a-6cd26871659c",
   "metadata": {},
   "outputs": [],
   "source": [
    "xab.x.units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab012bd2-29c1-4ace-8586-740b019c076a",
   "metadata": {},
   "outputs": [],
   "source": [
    "hv_dsb = hv.Dataset(xab)\n",
    "hv_imb = hv_dsb.to(hv.Image, kdims=[\"x\", \"y\"])\n",
    "hv_imb.opts(colorbar=True,\n",
    "                    width=500,\n",
    "                    height=400,\n",
    "                    cmap='seismic',\n",
    "                    clabel=f\"{xab.name} ({xab.units})\",)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f618f35-16bc-4350-a215-0e1fbc8cae68",
   "metadata": {},
   "source": [
    "## Cube format as the exception"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d4e108a-a966-46c1-b6ef-ddf946d09ac6",
   "metadata": {},
   "source": [
    "The only (known) exception to this rule is the `cube` format. The format itself [specifies that the values should be calculated in atomic units](http://paulbourke.net/dataformats/cube/) (if the number of voxels in a dimension is positive, which to the best of our knowledge, is always the case in Octopus 12.1). Thus, the `cube` output will always be in bohr, independently of what the `UnitsOutput` parameter says.\n",
    "Actually, `xcrysden` has also an [analogous specification](http://www.xcrysden.org/doc/XSF.html) but is ignored by octopus for now. So, we will read `xcrysden` with the unit that the user specifies in the `inp`, but this [may change in the future](https://gitlab.com/octopus-code/octopus/-/issues/592)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19790ddf-dc60-4c6d-a02c-6da9493e1a7c",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa_cube = runb.default.scf.density.get_converged(source=\"cube\") \n",
    "xa_cube"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5459f4d3-84e8-41a7-b285-feeca5e995e0",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa_cube.units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74c5508d-154e-4a7f-9ed7-7125b88b9fe9",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa_cube.x.units"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55d6d270-7a42-42fe-b606-553ab097754e",
   "metadata": {},
   "source": [
    "We will clearly see that the scaling is different in comparison to the last plot, due to the change of units:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "512c5ceb-eaa5-473e-a401-9a082bb59145",
   "metadata": {},
   "outputs": [],
   "source": [
    "hv_ds_cube = hv.Dataset(xa_cube)\n",
    "hv_im_cube = hv_ds_cube.to(hv.Image, kdims=[\"x\", \"y\"])\n",
    "hv_im_cube.opts(colorbar=True,\n",
    "                    width=500,\n",
    "                    height=400,\n",
    "                    cmap='seismic',\n",
    "                    clabel=f\"{xa_cube.name} ({xa_cube.units})\",)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbd77ddb",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
