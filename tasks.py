import os
import shutil

import invoke
import toml


@invoke.task
def release(ctx, test=True):
    """Pushes the package to pypi

    Steps involved:
    1. Prevent pushing to official PyPI if not in main branch
    2. Pull the latest changes from the remote
    3. Runs the tests
    4. Make the docs
    5. Builds the package and checks if it is valid
    6. Tag the current commit with the version number
    7. Uploads the package to pypi
    8. Push the tag to remote
    """
    # branches (appart from main) can only be published on TestPyPI
    branch = ctx.run("git branch --show-current", hide=True).stdout.strip()
    if branch != "main" and not test:
        raise invoke.Exit(
            "Not in main branch. Aborting release.\n\
            either switch to main branch or \n\
            push to test pypi using `--test=False` flag "
        )

    ctx.run("git pull")
    ctx.run("pytest")
    ctx.run("cd docs && make html && cd ..")

    if os.path.exists("dist"):
        shutil.rmtree("dist")

    ctx.run("python3 -m build")
    ctx.run("twine check dist/*")

    if not test:
        version = toml.load("pyproject.toml")["project"]["version"]

        ctx.run(f"git tag -a {version} -m 'Release {version}'")

    if test:
        repo_flag = "--repository-url https://test.pypi.org/legacy/ "
    else:
        repo_flag = ""

    ctx.run(f"twine upload {repo_flag} dist/*")

    if not test:
        ctx.run(f"git push origin {version}")  # push tag
